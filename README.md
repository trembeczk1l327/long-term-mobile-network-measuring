The scope of this app is the following:
Measure the available mobile network and follow the movements of the mobile device.
Results are stored into a SQL database.
The GPS and the mobile datas are provided by broadcast receiver. The received level is shown in a chart.
The speedtest is done by download and upload a file on a FTP server. The speed is counted from the filesize and elapsed time.
The ping is provided by ping service.